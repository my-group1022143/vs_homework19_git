#include<iostream>

class Animal
{
public:
	virtual void Voice() const = 0;
};


void Animal::Voice() const
{
	std::cout << "Animal voice" << std::endl;
}


class Dog:public Animal
{
public:
	void Voice() const override
	{
		std::cout << "Woof!" << std::endl;
	}
};

class Cat :public Animal
{
public:
	void Voice() const override
	{
		std::cout << "Meow!" << std::endl;
	}
};

class Duck :public Animal
{
public:
	void Voice() const override
	{
		std::cout << "Quack!" << std::endl;
	}
};

int main()
{
	Animal* animals[] = { new Dog(), new Cat(), new Duck() };
	for (auto animalPtr : animals) 
	{ 
		animalPtr->Voice();
		delete animalPtr;
	}
	return 0;
}